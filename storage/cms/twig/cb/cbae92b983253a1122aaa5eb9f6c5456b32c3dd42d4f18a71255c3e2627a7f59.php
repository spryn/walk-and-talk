<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/layouts/blog-post.htm */
class __TwigTemplate_70ca373d3f4bd6e791f635822eb68642f1c11038393403a87010768983d28ff9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0\">
    <meta name=\"description\" content=\"I believe that everyone has the potential to have good mental health and that they know themselves best and when given the right support they are able to achieve personal growth.\"/>
    <meta name=\"keywords\"
          content=\"\"/>
    <meta name=\"name\" itemprop=\"name\" content=\"http://katebulltherapy.com\">

    <title>";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo " | Kate Bull Therapy</title>

    <link rel=\"shortcut icon\" href=\"";
        // line 14
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/favicon.ico");
        echo "\" type=\"image/x-icon\">
    <link rel=\"icon\" href=\"";
        // line 15
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/favicon.ico");
        echo "\" type=\"image/x-icon\">

    <link rel=\"stylesheet\" href=\"";
        // line 17
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/font-awesome.min.css");
        echo "\">
    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\">
    <link href=\"https://fonts.googleapis.com/css?family=Lora:700|Work+Sans:300,400\" rel=\"stylesheet\">

    <!-- Custom CSS -->
    <link rel=\"stylesheet\" href=\"";
        // line 22
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/app.css");
        echo "\">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->

</head>

<body>

";
        // line 35
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("nav"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 36
        echo "
";
        // line 37
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post-header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 38
        echo "
";
        // line 39
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 40
        echo "
";
        // line 41
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 42
        echo "
<!-- Scripts -->
<script src=\"";
        // line 44
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/javascript/app.js");
        echo "\"></script>
";
        // line 45
        echo '<script src="'. Request::getBasePath()
                .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
        echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras.css">'.PHP_EOL;
        // line 46
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 47
        echo "
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/layouts/blog-post.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 47,  108 => 46,  101 => 45,  97 => 44,  93 => 42,  89 => 41,  86 => 40,  84 => 39,  81 => 38,  77 => 37,  74 => 36,  70 => 35,  54 => 22,  46 => 17,  41 => 15,  37 => 14,  32 => 12,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0\">
    <meta name=\"description\" content=\"I believe that everyone has the potential to have good mental health and that they know themselves best and when given the right support they are able to achieve personal growth.\"/>
    <meta name=\"keywords\"
          content=\"\"/>
    <meta name=\"name\" itemprop=\"name\" content=\"http://katebulltherapy.com\">

    <title>{{ this.page.title }} | Kate Bull Therapy</title>

    <link rel=\"shortcut icon\" href=\"{{ 'assets/img/favicon.ico'|theme }}\" type=\"image/x-icon\">
    <link rel=\"icon\" href=\"{{ 'assets/img/favicon.ico'|theme }}\" type=\"image/x-icon\">

    <link rel=\"stylesheet\" href=\"{{ 'assets/css/font-awesome.min.css'|theme }}\">
    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\">
    <link href=\"https://fonts.googleapis.com/css?family=Lora:700|Work+Sans:300,400\" rel=\"stylesheet\">

    <!-- Custom CSS -->
    <link rel=\"stylesheet\" href=\"{{ 'assets/css/app.css'|theme }}\">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->

</head>

<body>

{% partial 'nav' %}

{% partial 'blog/post-header' %}

{% page %}

{% partial 'footer' %}

<!-- Scripts -->
<script src=\"{{ 'assets/javascript/app.js'|theme }}\"></script>
{% framework extras %}
{% scripts %}

</body>
</html>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/layouts/blog-post.htm", "");
    }
}
