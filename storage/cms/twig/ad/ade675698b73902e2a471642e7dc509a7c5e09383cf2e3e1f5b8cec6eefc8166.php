<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/blog/header.htm */
class __TwigTemplate_58e252f52f71d47fe4199e69e93787146919d74815c55a6ecd18a4cc3b2b50ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"header header-blog\">
    <div class=\"text-center d-flex\">
        <div class=\"container my-auto\">
            <div class=\"row\">
                <div class=\"col-12 mx-auto\">
                    <div class=\"header-title-home\">Blog</div>
                </div>
            </div>
        </div>
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/blog/header.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"header header-blog\">
    <div class=\"text-center d-flex\">
        <div class=\"container my-auto\">
            <div class=\"row\">
                <div class=\"col-12 mx-auto\">
                    <div class=\"header-title-home\">Blog</div>
                </div>
            </div>
        </div>
    </div>
</header>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/blog/header.htm", "");
    }
}
