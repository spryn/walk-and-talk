<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/event-details.htm */
class __TwigTemplate_a29e0f544bf8fc4e77edc362e1fac1f24293beef465d683aa1fe54847b7039d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["record"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderDetails"] ?? null), "record", array());
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderDetails"] ?? null), "displayColumn", array());
        // line 3
        $context["notFoundMessage"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderDetails"] ?? null), "notFoundMessage", array());
        // line 4
        echo "
";
        // line 5
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("events/post-header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 6
        echo "
<section id=\"event-page\">
    <div class=\"container\">
        ";
        // line 9
        if (($context["record"] ?? null)) {
            // line 10
            echo "
        <h1 class=\"mb-3 h1\">";
            // line 11
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["record"] ?? null), "date", array()), "d-m-Y"), "html", null, true);
            echo "</h1>

        <div class=\"mt-5\">
            <div class=\"content\">
                ";
            // line 15
            echo twig_get_attribute($this->env, $this->getSourceContext(), ($context["record"] ?? null), "description", array());
            echo "
            </div>
        </div>

        ";
        }
        // line 20
        echo "    </div>
</section>

<section id=\"sharing\">
    <div class=\"container\">
        <div class=\"blog-share-section text-center mt-5\">
            <div class=\"col-xl-12\">
                <h1 class=\"title\">Share this post:</h1>
                <div class=\"row mt-5\">
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://www.facebook.com/sharer/sharer.php?u=";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "url", array()), "html", null, true);
        echo "\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"";
        // line 33
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/icons/facebook.svg");
        echo "\" alt=\"Facebook\">
                                    <h2>Facebook</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 mt-3 mt-sm-0\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://twitter.com/home?status=";
        // line 41
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "url", array()), "html", null, true);
        echo "\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"";
        // line 43
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/icons/twitter.svg");
        echo "\" alt=\"Twitter\">
                                    <h2>Twitter</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 mt-3 mt-sm-3 mt-md-0\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://plus.google.com/share?url=";
        // line 51
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "url", array()), "html", null, true);
        echo "\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"";
        // line 53
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/icons/google.svg");
        echo "\" alt=\"Google\">
                                    <h2>Google</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 mt-3 mt-sm-3 mt-md-0\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://www.linkedin.com/shareArticle?mini=true&url=";
        // line 61
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "url", array()), "html", null, true);
        echo "&title=";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "title", array()), "html", null, true);
        echo "&summary=&source=\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"";
        // line 63
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/icons/linkedin.svg");
        echo "\" alt=\"LinkedIn\">
                                    <h2>LinkedIn</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    document.getElementById(\"url\").innerHTML = window.location.href;
</script>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/event-details.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 63,  118 => 61,  107 => 53,  102 => 51,  91 => 43,  86 => 41,  75 => 33,  70 => 31,  57 => 20,  49 => 15,  42 => 11,  39 => 10,  37 => 9,  32 => 6,  28 => 5,  25 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set record = builderDetails.record %}
{% set displayColumn = builderDetails.displayColumn %}
{% set notFoundMessage = builderDetails.notFoundMessage %}

{% partial 'events/post-header' %}

<section id=\"event-page\">
    <div class=\"container\">
        {% if record %}

        <h1 class=\"mb-3 h1\">{{record.date|date(\"d-m-Y\")}}</h1>

        <div class=\"mt-5\">
            <div class=\"content\">
                {{record.description|raw}}
            </div>
        </div>

        {% endif %}
    </div>
</section>

<section id=\"sharing\">
    <div class=\"container\">
        <div class=\"blog-share-section text-center mt-5\">
            <div class=\"col-xl-12\">
                <h1 class=\"title\">Share this post:</h1>
                <div class=\"row mt-5\">
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://www.facebook.com/sharer/sharer.php?u={{ post.url }}\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"{{ 'assets/img/icons/facebook.svg'|theme }}\" alt=\"Facebook\">
                                    <h2>Facebook</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 mt-3 mt-sm-0\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://twitter.com/home?status={{ post.url }}\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"{{ 'assets/img/icons/twitter.svg'|theme }}\" alt=\"Twitter\">
                                    <h2>Twitter</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 mt-3 mt-sm-3 mt-md-0\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://plus.google.com/share?url={{ post.url }}\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"{{ 'assets/img/icons/google.svg'|theme }}\" alt=\"Google\">
                                    <h2>Google</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 mt-3 mt-sm-3 mt-md-0\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://www.linkedin.com/shareArticle?mini=true&url={{ post.url }}&title={{ post.title }}&summary=&source=\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"{{ 'assets/img/icons/linkedin.svg'|theme }}\" alt=\"LinkedIn\">
                                    <h2>LinkedIn</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    document.getElementById(\"url\").innerHTML = window.location.href;
</script>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/event-details.htm", "");
    }
}
