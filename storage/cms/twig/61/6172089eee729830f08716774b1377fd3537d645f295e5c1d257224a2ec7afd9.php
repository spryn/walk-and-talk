<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/blog.htm */
class __TwigTemplate_ddb6aa6db43019372fd404df73acac3eb05e5cce3642c19d6b9aee1557ab37e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"blog-list\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-12 col-md-10 mx-auto\">

                ";
        // line 6
        $context["posts"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["blogPosts"] ?? null), "posts", array());
        // line 7
        echo "                <div class=\"row\">
                    ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 9
            echo "                    <div class=\"col-md-12 col-xl-6\">
                        <div class=\"post mb-5\">
                            <div class=\"blog-image-container\">
                                <a href=\"";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "url", array()), "html", null, true);
            echo "\">
                                    <div style=\"background-image: url('";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "featured_images", array()), 0, array(), "array"), "path", array()), "html", null, true);
            echo "');\" class=\"blog-image\"></div>
                                </a>
                            </div>
                            <div class=\"post-info text-center\">
                                <h2 class=\"section-heading\"><a href=\"";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "url", array()), "html", null, true);
            echo "\" class=\"a-indoor\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "title", array()), "html", null, true);
            echo "</a></h2>
                                <ul class=\"list-inline blog-info\">
                                    <li class=\"blog-author list-inline-item\">";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "user", array()), "first_name", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "user", array()), "last_name", array()), "html", null, true);
            echo "</li>
                                    <li class=\"list-inline-item\">-</li>
                                    <li class=\"blog-date list-inline-item\">";
            // line 21
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "published_at", array()), "M d, Y"), "html", null, true);
            echo "</li>
                                    <li class=\"list-inline-item\">-</li>
                                    <li class=\"blog-category list-inline-item\">
                                        ";
            // line 24
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "categories", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 25
                echo "                                        <a href=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["category"], "url", array()), "html", null, true);
                echo "\" class=\"a-indoor\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["category"], "name", array()), "html", null, true);
                echo "</a>";
                if ( !twig_get_attribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 26
                echo "                                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "                                    </li>
                                </ul>
                                <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "url", array()), "html", null, true);
            echo "\" class=\"btn btn-custom mt-3\">View</a>
                            </div>
                        </div>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "                </div>

            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/blog.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 34,  115 => 29,  111 => 27,  97 => 26,  88 => 25,  71 => 24,  65 => 21,  58 => 19,  51 => 17,  44 => 13,  40 => 12,  35 => 9,  31 => 8,  28 => 7,  26 => 6,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"blog-list\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-12 col-md-10 mx-auto\">

                {% set posts = blogPosts.posts %}
                <div class=\"row\">
                    {% for post in posts %}
                    <div class=\"col-md-12 col-xl-6\">
                        <div class=\"post mb-5\">
                            <div class=\"blog-image-container\">
                                <a href=\"{{ post.url }}\">
                                    <div style=\"background-image: url('{{post.featured_images[0].path}}');\" class=\"blog-image\"></div>
                                </a>
                            </div>
                            <div class=\"post-info text-center\">
                                <h2 class=\"section-heading\"><a href=\"{{ post.url }}\" class=\"a-indoor\">{{ post.title }}</a></h2>
                                <ul class=\"list-inline blog-info\">
                                    <li class=\"blog-author list-inline-item\">{{ post.user.first_name }} {{ post.user.last_name }}</li>
                                    <li class=\"list-inline-item\">-</li>
                                    <li class=\"blog-date list-inline-item\">{{ post.published_at|date('M d, Y') }}</li>
                                    <li class=\"list-inline-item\">-</li>
                                    <li class=\"blog-category list-inline-item\">
                                        {% for category in post.categories %}
                                        <a href=\"{{ category.url }}\" class=\"a-indoor\">{{ category.name }}</a>{% if not loop.last %}, {% endif %}
                                        {% endfor %}
                                    </li>
                                </ul>
                                <a href=\"{{ post.url }}\" class=\"btn btn-custom mt-3\">View</a>
                            </div>
                        </div>
                    </div>
                    {% endfor %}
                </div>

            </div>
        </div>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/blog.htm", "");
    }
}
