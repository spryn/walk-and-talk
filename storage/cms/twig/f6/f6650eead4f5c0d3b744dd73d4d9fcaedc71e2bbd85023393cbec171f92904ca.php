<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/home/issues.htm */
class __TwigTemplate_3e1f65a488a0d4d93314acacc874420d02e732d48632e41b773fd21e41eba47b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"issues\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12\">
                <h1 class=\"mb-3 h1\">Issues I Deal With</h1>
                <div class=\"row\">
                    <div class=\"col-12 col-md-4\">
                        <ul class=\"mb-0\">
                            <li>Abortion</li>
                            <li>Abuse</li>
                            <li>Addiction(s)</li>
                            <li>Affairs and betrayals</li>
                            <li>Anger management</li>
                            <li>Anxiety</li>
                            <li>Bereavement</li>
                            <li>Bullying</li>
                            <li>Cancer</li>
                            <li>Career</li>
                            <li>Carer support</li>
                            <li>Child related issues</li>
                        </ul>
                    </div>
                    <div class=\"col-12 col-md-4\">
                        <ul class=\"mb-0\">
                            <li>Depression</li>
                            <li>Domestic violence</li>
                            <li>Eating disorders</li>
                            <li>Emotional abuse</li>
                            <li>Family issues</li>
                            <li>HIV/AIDS</li>
                            <li>Infertility</li>
                            <li>Low self-confidence</li>
                            <li>Low self-esteem</li>
                            <li>Miscarriage</li>
                            <li>Passive aggressive behaviour</li>
                            <li>Physical abuse</li>
                        </ul>
                    </div>
                    <div class=\"col-12 col-md-4\">
                        <ul class=\"mb-0\">
                            <li>Post-traumatic stress disorder (PTSD)</li>
                            <li>Postnatal depression</li>
                            <li>Redundancy</li>
                            <li>Relationship issues</li>
                            <li>Self-harm</li>
                            <li>Separation and divorce</li>
                            <li>Sexual abuse</li>
                            <li>Spirituality</li>
                            <li>Stress</li>
                            <li>Suicidal thoughts</li>
                            <li>Trauma</li>
                            <li>Work-related stress</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/home/issues.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"issues\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12\">
                <h1 class=\"mb-3 h1\">Issues I Deal With</h1>
                <div class=\"row\">
                    <div class=\"col-12 col-md-4\">
                        <ul class=\"mb-0\">
                            <li>Abortion</li>
                            <li>Abuse</li>
                            <li>Addiction(s)</li>
                            <li>Affairs and betrayals</li>
                            <li>Anger management</li>
                            <li>Anxiety</li>
                            <li>Bereavement</li>
                            <li>Bullying</li>
                            <li>Cancer</li>
                            <li>Career</li>
                            <li>Carer support</li>
                            <li>Child related issues</li>
                        </ul>
                    </div>
                    <div class=\"col-12 col-md-4\">
                        <ul class=\"mb-0\">
                            <li>Depression</li>
                            <li>Domestic violence</li>
                            <li>Eating disorders</li>
                            <li>Emotional abuse</li>
                            <li>Family issues</li>
                            <li>HIV/AIDS</li>
                            <li>Infertility</li>
                            <li>Low self-confidence</li>
                            <li>Low self-esteem</li>
                            <li>Miscarriage</li>
                            <li>Passive aggressive behaviour</li>
                            <li>Physical abuse</li>
                        </ul>
                    </div>
                    <div class=\"col-12 col-md-4\">
                        <ul class=\"mb-0\">
                            <li>Post-traumatic stress disorder (PTSD)</li>
                            <li>Postnatal depression</li>
                            <li>Redundancy</li>
                            <li>Relationship issues</li>
                            <li>Self-harm</li>
                            <li>Separation and divorce</li>
                            <li>Sexual abuse</li>
                            <li>Spirituality</li>
                            <li>Stress</li>
                            <li>Suicidal thoughts</li>
                            <li>Trauma</li>
                            <li>Work-related stress</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/home/issues.htm", "");
    }
}
