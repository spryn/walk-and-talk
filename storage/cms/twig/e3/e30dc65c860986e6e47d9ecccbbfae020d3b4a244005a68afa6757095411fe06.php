<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/contact.htm */
class __TwigTemplate_804e3fd67b183e7e5882b76f136606aecfea7a6dca329e291340df675980f0ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"contact-form\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12\">
                <h1 class=\"mb-3 h1\">Get in touch!</h1>
                <div class=\"form\">
                    ";
        // line 7
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("emptyForm"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 8
        echo "                </div>
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/contact.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 8,  27 => 7,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"contact-form\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12\">
                <h1 class=\"mb-3 h1\">Get in touch!</h1>
                <div class=\"form\">
                    {% component 'emptyForm' %}
                </div>
            </div>
        </div>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/contact.htm", "");
    }
}
