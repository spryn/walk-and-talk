<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/issues/issues.htm */
class __TwigTemplate_18f80310d371be9f997e98443c25ffb30c8090dc4c579025994eee5ad9adb270 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"issues-list\">
    <div class=\"container\">
        <h1 class=\"mb-3 h1\">What other issues do I counsel?</h1>
        <div class=\"row\">
            <div class=\"col-12 col-md-4\">
                <ul class=\"mb-0\">
                    <li>Abortion</li>
                    <li>Abuse</li>
                    <li>Addiction(s)</li>
                    <li>Affairs and betrayals</li>
                    <li>Anger management</li>
                    <li>Bereavement</li>
                    <li>Bullying</li>
                    <li>Cancer</li>
                    <li>Career</li>
                    <li>Carer support</li>
                    <li>Child related issues</li>
                    <li>Domestic violence</li>
                </ul>
            </div>
            <div class=\"col-12 col-md-4\">
                <ul class=\"mb-0\">
                    <li>Eating disorders</li>
                    <li>Emotional abuse</li>
                    <li>Family issues</li>
                    <li>HIV/AIDS</li>
                    <li>Infertility</li>
                    <li>Low self-confidence</li>
                    <li>Low self-esteem</li>
                    <li>Miscarriage</li>
                    <li>Passive aggressive behaviour</li>
                    <li>Physical abuse</li>
                    <li>Post-traumatic stress disorder (PTSD)</li>
                    <li>Postnatal depression</li>
                </ul>
            </div>
            <div class=\"col-12 col-md-4\">
                <ul class=\"mb-0\">
                    <li>Redundancy</li>
                    <li>Relationship issues</li>
                    <li>Self-harm</li>
                    <li>Separation and divorce</li>
                    <li>Sexual abuse</li>
                    <li>Spirituality</li>
                    <li>Stress</li>
                    <li>Suicidal thoughts</li>
                    <li>Trauma</li>
                    <li>Work-related stress</li>
                </ul>
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/issues/issues.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"issues-list\">
    <div class=\"container\">
        <h1 class=\"mb-3 h1\">What other issues do I counsel?</h1>
        <div class=\"row\">
            <div class=\"col-12 col-md-4\">
                <ul class=\"mb-0\">
                    <li>Abortion</li>
                    <li>Abuse</li>
                    <li>Addiction(s)</li>
                    <li>Affairs and betrayals</li>
                    <li>Anger management</li>
                    <li>Bereavement</li>
                    <li>Bullying</li>
                    <li>Cancer</li>
                    <li>Career</li>
                    <li>Carer support</li>
                    <li>Child related issues</li>
                    <li>Domestic violence</li>
                </ul>
            </div>
            <div class=\"col-12 col-md-4\">
                <ul class=\"mb-0\">
                    <li>Eating disorders</li>
                    <li>Emotional abuse</li>
                    <li>Family issues</li>
                    <li>HIV/AIDS</li>
                    <li>Infertility</li>
                    <li>Low self-confidence</li>
                    <li>Low self-esteem</li>
                    <li>Miscarriage</li>
                    <li>Passive aggressive behaviour</li>
                    <li>Physical abuse</li>
                    <li>Post-traumatic stress disorder (PTSD)</li>
                    <li>Postnatal depression</li>
                </ul>
            </div>
            <div class=\"col-12 col-md-4\">
                <ul class=\"mb-0\">
                    <li>Redundancy</li>
                    <li>Relationship issues</li>
                    <li>Self-harm</li>
                    <li>Separation and divorce</li>
                    <li>Sexual abuse</li>
                    <li>Spirituality</li>
                    <li>Stress</li>
                    <li>Suicidal thoughts</li>
                    <li>Trauma</li>
                    <li>Work-related stress</li>
                </ul>
            </div>
        </div>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/issues/issues.htm", "");
    }
}
