<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/events/post-header.htm */
class __TwigTemplate_13ecef296764cdd334d2326c9c30a52291eff66db957c1b43fef5fdce154ff4a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((null === twig_get_attribute($this->env, $this->getSourceContext(), ($context["record"] ?? null), "event_image", array()))) {
            // line 2
            echo "<header class=\"header text-center d-flex\" style=\"background: url(";
            echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/bg/header-home.jpg");
            echo ") center center no-repeat; background-size: cover;\">
    ";
        } else {
            // line 4
            echo "    <header class=\"header text-center d-flex\" style=\"background: url(";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["record"] ?? null), "event_image", array()), "path", array()), "html", null, true);
            echo ") center center no-repeat; background-size: cover;\">
        ";
        }
        // line 6
        echo "        <div class=\"container my-auto\">
            <div class=\"row\">
                <div class=\"col-xl-10 mx-auto\">
                    <div class=\"header-title\"> ";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["record"] ?? null), "name", array()), "html", null, true);
        echo "</div>
                </div>
            </div>
        </div>
    </header>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/events/post-header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 9,  33 => 6,  27 => 4,  21 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if record.event_image is null %}
<header class=\"header text-center d-flex\" style=\"background: url({{ 'assets/img/bg/header-home.jpg'|theme }}) center center no-repeat; background-size: cover;\">
    {% else %}
    <header class=\"header text-center d-flex\" style=\"background: url({{ record.event_image.path }}) center center no-repeat; background-size: cover;\">
        {% endif %}
        <div class=\"container my-auto\">
            <div class=\"row\">
                <div class=\"col-xl-10 mx-auto\">
                    <div class=\"header-title\"> {{ record.name }}</div>
                </div>
            </div>
        </div>
    </header>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/events/post-header.htm", "");
    }
}
