<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/contact/header.htm */
class __TwigTemplate_a92afe4350836544556f4d78126c2a44c6cb3202a1ed21223f96291468b20e5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"header header-contact\">
    <div class=\"text-center d-flex\">
        <div class=\"container my-auto\">
            <div class=\"row\">
                <div class=\"col-12 mx-auto\">
                    <div class=\"header-title-home\">Contact</div>
                </div>
            </div>
        </div>
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/contact/header.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"header header-contact\">
    <div class=\"text-center d-flex\">
        <div class=\"container my-auto\">
            <div class=\"row\">
                <div class=\"col-12 mx-auto\">
                    <div class=\"header-title-home\">Contact</div>
                </div>
            </div>
        </div>
    </div>
</header>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/contact/header.htm", "");
    }
}
