<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/footer.htm */
class __TwigTemplate_cf5d31ad214bb6e97b25995290cf986a2efb61286cb9ab4747e9a65dddb36e53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer>
    <div class=\"container\">
        <div class=\"footer-logo mb-5\">
            <img src=\"";
        // line 4
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/logo/logo.svg");
        echo "\" alt=\"Walk & Talk North East\">
        </div>
        <div class=\"row\">
            <div class=\"col-lg-4 text-center d-flex align-items-center footer-border-right\">
                <div class=\"footer-call mx-auto\">
                    <h2>Call us:</h2>
                    <p><a href=\"tel://07958787514\" class=\"footer-link a-indoor\">07958787514</a></p>
                </div>
            </div>
            <div class=\"col-lg-4 text-center d-flex align-items-center footer-border-right\">
                <div class=\"footer-find mx-auto\">
                    <h2>Find us:</h2>
                    <ul class=\"list-inline mb-0 mt-1\">
                        <li class=\"list-inline-item footer-social-media-icon\">
                            <a href=\"https://www.facebook.com/walkandtalknortheast/\" target=\"_blank\" class=\"a-indoor\">
                                <i class=\"fa fa-facebook-official fa-2x\"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class=\"col-lg-4 text-center d-flex align-items-center\">
                <div class=\"footer-email mx-auto\">
                    <h2>Email us:</h2>
                    <p><a href=\"mailto:info@walkandtalknortheast.com\" class=\"footer-link a-indoor\">info@walkandtalknortheast.com</a></p>
                </div>
            </div>
        </div>
        <div class=\"text-center mt-5 mb-5\">
            <img src=\"";
        // line 33
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/bacp.png");
        echo "\" class=\"footer-bacp\" alt=\"bacp registered member\">
        </div>
        <div class=\"footer-info text-center\">
            <ul class=\"list-inline\">
                <li class=\"list-inline-item\">Copyright © 2017. Walk & Talk North East.
                </li>
                <li class=\"list-inline-item hidden-xs\">-
                </li>
                <li class=\"list-inline-item\">
                    <a href=\"/terms-and-privacy\" class=\"footer-link a-indoor\">Terms of Service & Privacy Policy</a>
                </li>
                <li class=\"list-inline-item hidden-xs\">-
                </li>
                <li class=\"list-inline-item\">
                    <a href=\"/cookie-policy\" class=\"footer-link a-indoor\">Cookie Policy</a>
                </li>
                <li class=\"list-inline-item hidden-xs\">-
                <li class=\"list-inline-item\">Designed by <a href=\"https://bluecookie.co.uk\" class=\"footer-link a-indoor\">BlueCookie</a></li>
            </ul>
        </div>
    </div>
</footer>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 33,  24 => 4,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<footer>
    <div class=\"container\">
        <div class=\"footer-logo mb-5\">
            <img src=\"{{ 'assets/img/logo/logo.svg'|theme }}\" alt=\"Walk & Talk North East\">
        </div>
        <div class=\"row\">
            <div class=\"col-lg-4 text-center d-flex align-items-center footer-border-right\">
                <div class=\"footer-call mx-auto\">
                    <h2>Call us:</h2>
                    <p><a href=\"tel://07958787514\" class=\"footer-link a-indoor\">07958787514</a></p>
                </div>
            </div>
            <div class=\"col-lg-4 text-center d-flex align-items-center footer-border-right\">
                <div class=\"footer-find mx-auto\">
                    <h2>Find us:</h2>
                    <ul class=\"list-inline mb-0 mt-1\">
                        <li class=\"list-inline-item footer-social-media-icon\">
                            <a href=\"https://www.facebook.com/walkandtalknortheast/\" target=\"_blank\" class=\"a-indoor\">
                                <i class=\"fa fa-facebook-official fa-2x\"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class=\"col-lg-4 text-center d-flex align-items-center\">
                <div class=\"footer-email mx-auto\">
                    <h2>Email us:</h2>
                    <p><a href=\"mailto:info@walkandtalknortheast.com\" class=\"footer-link a-indoor\">info@walkandtalknortheast.com</a></p>
                </div>
            </div>
        </div>
        <div class=\"text-center mt-5 mb-5\">
            <img src=\"{{ 'assets/img/bacp.png'|theme }}\" class=\"footer-bacp\" alt=\"bacp registered member\">
        </div>
        <div class=\"footer-info text-center\">
            <ul class=\"list-inline\">
                <li class=\"list-inline-item\">Copyright © 2017. Walk & Talk North East.
                </li>
                <li class=\"list-inline-item hidden-xs\">-
                </li>
                <li class=\"list-inline-item\">
                    <a href=\"/terms-and-privacy\" class=\"footer-link a-indoor\">Terms of Service & Privacy Policy</a>
                </li>
                <li class=\"list-inline-item hidden-xs\">-
                </li>
                <li class=\"list-inline-item\">
                    <a href=\"/cookie-policy\" class=\"footer-link a-indoor\">Cookie Policy</a>
                </li>
                <li class=\"list-inline-item hidden-xs\">-
                <li class=\"list-inline-item\">Designed by <a href=\"https://bluecookie.co.uk\" class=\"footer-link a-indoor\">BlueCookie</a></li>
            </ul>
        </div>
    </div>
</footer>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/footer.htm", "");
    }
}
