<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/home/outdoor.htm */
class __TwigTemplate_236211e81b43ea2b215fd47a0f94d474bc8229acae5641b0db02bb8650380575 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"outdoor-sessions\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12\">
                <h1 class=\"mb-3 h1\">Outdoor Sessions</h1>
                ";
        // line 6
        echo ($context["outdoor_message"] ?? null);
        echo "
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/home/outdoor.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 6,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"outdoor-sessions\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12\">
                <h1 class=\"mb-3 h1\">Outdoor Sessions</h1>
                {{outdoor_message| raw}}
            </div>
        </div>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/home/outdoor.htm", "");
    }
}
