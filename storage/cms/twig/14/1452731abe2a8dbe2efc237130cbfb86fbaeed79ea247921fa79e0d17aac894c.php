<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/issues/header.htm */
class __TwigTemplate_721638d0b26b695ffdbe7d5c5306adc57c9c471567ebe46dbc46f5a46f8ff747 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"header header-issues\">
    <div class=\"text-center d-flex\">
        <div class=\"container my-auto\">
            <div class=\"row\">
                <div class=\"col-12 mx-auto\">
                    <div class=\"header-title-home\">Key Issues</div>
                </div>
            </div>
        </div>
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/issues/header.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"header header-issues\">
    <div class=\"text-center d-flex\">
        <div class=\"container my-auto\">
            <div class=\"row\">
                <div class=\"col-12 mx-auto\">
                    <div class=\"header-title-home\">Key Issues</div>
                </div>
            </div>
        </div>
    </div>
</header>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/issues/header.htm", "");
    }
}
