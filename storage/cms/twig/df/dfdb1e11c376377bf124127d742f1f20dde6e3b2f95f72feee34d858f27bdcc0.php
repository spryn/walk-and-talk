<?php

/* /home/vagrant/projects/walk-and-talk/plugins/martin/forms/components/partials/js/recaptcha.js */
class __TwigTemplate_86e69bacc95da5bdc150727423fc6cfd96cc627ace37d16a30bd104a0c6ebfbb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "resetReCaptcha('";
        echo twig_escape_filter($this->env, ($context["__SELF__"] ?? null), "html", null, true);
        echo "');";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/plugins/martin/forms/components/partials/js/recaptcha.js";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("resetReCaptcha('{{ __SELF__ }}');", "/home/vagrant/projects/walk-and-talk/plugins/martin/forms/components/partials/js/recaptcha.js", "");
    }
}
