<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/nav.htm */
class __TwigTemplate_90c921d65a8ff88ea8264a96b058ca6da4fe68a744abc1e5cb4a156659830d08 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav class=\"navbar navbar-expand-lg navbar-light fixed-top\" id=\"mainNav\">
    <div class=\"container\">
        <a href=\"/\">
            <img src=\"";
        // line 4
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/logo/logo.svg");
        echo "\" class=\"nav-logo\" alt=\"Logo\">
        </a>
        <button id=\"button\" class=\"navbar-toggler navbar-toggler-right hamburger d-lg-none d-xl-none\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\"
                aria-expanded=\"false\"
                aria-label=\"Toggle navigation\">
            <span>Menu</span>
            <i class=\"material-icons\">menu</i>
        </button>
        <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
            <ul class=\"navbar-nav ml-auto\">
                <li class=\"nav-item\">
                    ";
        // line 15
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "url", array()) == "/")) {
            // line 16
            echo "                    <a class=\"active nav-link\" href=\"/\">Home</a>
                    ";
        } else {
            // line 18
            echo "                    <a class=\"nav-link\" href=\"/\">Home</a>
                    ";
        }
        // line 20
        echo "                </li>
                <li class=\"nav-item\">
                    ";
        // line 22
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "url", array()) == "/key-issues")) {
            // line 23
            echo "                    <a class=\"active nav-link\" href=\"/key-issues\">Key Issues</a>
                    ";
        } else {
            // line 25
            echo "                    <a class=\"nav-link\" href=\"/key-issues\">Key Issues</a>
                    ";
        }
        // line 27
        echo "                </li>
                <li class=\"nav-item\">
                    ";
        // line 29
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "url", array()) == "/outdoor-sessions")) {
            // line 30
            echo "                    <a class=\"active nav-link\" href=\"/outdoor-sessions\">Outdoor Sessions</a>
                    ";
        } else {
            // line 32
            echo "                    <a class=\"nav-link\" href=\"/outdoor-sessions\">Outdoor Sessions</a>
                    ";
        }
        // line 34
        echo "                </li>
                <li class=\"nav-item\">
                    ";
        // line 36
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "id", array()) == "events")) {
            // line 37
            echo "                    <a class=\"active nav-link\" href=\"/events\">Events</a>
                    ";
        } else {
            // line 39
            echo "                    <a class=\"nav-link\" href=\"/events\">Events</a>
                    ";
        }
        // line 41
        echo "                </li>
                <li class=\"nav-item\">
                    ";
        // line 43
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "id", array()) == "contact")) {
            // line 44
            echo "                    <a class=\"active nav-link\" href=\"/contact\">Contact</a>
                    ";
        } else {
            // line 46
            echo "                    <a class=\"nav-link\" href=\"/contact\">Contact</a>
                    ";
        }
        // line 48
        echo "                </li>
                <li class=\"nav-item\">
                    ";
        // line 50
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "id", array()) == "blog")) {
            // line 51
            echo "                    <a class=\"active nav-link\" href=\"/blog/all\">Blog</a>
                    ";
        } else {
            // line 53
            echo "                    <a class=\"nav-link\" href=\"/blog/all\">Blog</a>
                    ";
        }
        // line 55
        echo "                </li>
            </ul>
        </div>
    </div>
</nav>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/nav.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 55,  114 => 53,  110 => 51,  108 => 50,  104 => 48,  100 => 46,  96 => 44,  94 => 43,  90 => 41,  86 => 39,  82 => 37,  80 => 36,  76 => 34,  72 => 32,  68 => 30,  66 => 29,  62 => 27,  58 => 25,  54 => 23,  52 => 22,  48 => 20,  44 => 18,  40 => 16,  38 => 15,  24 => 4,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<nav class=\"navbar navbar-expand-lg navbar-light fixed-top\" id=\"mainNav\">
    <div class=\"container\">
        <a href=\"/\">
            <img src=\"{{ 'assets/img/logo/logo.svg'|theme }}\" class=\"nav-logo\" alt=\"Logo\">
        </a>
        <button id=\"button\" class=\"navbar-toggler navbar-toggler-right hamburger d-lg-none d-xl-none\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\" aria-controls=\"navbarResponsive\"
                aria-expanded=\"false\"
                aria-label=\"Toggle navigation\">
            <span>Menu</span>
            <i class=\"material-icons\">menu</i>
        </button>
        <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
            <ul class=\"navbar-nav ml-auto\">
                <li class=\"nav-item\">
                    {% if this.page.url == '/' %}
                    <a class=\"active nav-link\" href=\"/\">Home</a>
                    {% else %}
                    <a class=\"nav-link\" href=\"/\">Home</a>
                    {% endif %}
                </li>
                <li class=\"nav-item\">
                    {% if this.page.url == '/key-issues' %}
                    <a class=\"active nav-link\" href=\"/key-issues\">Key Issues</a>
                    {% else %}
                    <a class=\"nav-link\" href=\"/key-issues\">Key Issues</a>
                    {% endif %}
                </li>
                <li class=\"nav-item\">
                    {% if this.page.url == '/outdoor-sessions' %}
                    <a class=\"active nav-link\" href=\"/outdoor-sessions\">Outdoor Sessions</a>
                    {% else %}
                    <a class=\"nav-link\" href=\"/outdoor-sessions\">Outdoor Sessions</a>
                    {% endif %}
                </li>
                <li class=\"nav-item\">
                    {% if this.page.id == 'events' %}
                    <a class=\"active nav-link\" href=\"/events\">Events</a>
                    {% else %}
                    <a class=\"nav-link\" href=\"/events\">Events</a>
                    {% endif %}
                </li>
                <li class=\"nav-item\">
                    {% if this.page.id == 'contact' %}
                    <a class=\"active nav-link\" href=\"/contact\">Contact</a>
                    {% else %}
                    <a class=\"nav-link\" href=\"/contact\">Contact</a>
                    {% endif %}
                </li>
                <li class=\"nav-item\">
                    {% if this.page.id == 'blog' %}
                    <a class=\"active nav-link\" href=\"/blog/all\">Blog</a>
                    {% else %}
                    <a class=\"nav-link\" href=\"/blog/all\">Blog</a>
                    {% endif %}
                </li>
            </ul>
        </div>
    </div>
</nav>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/nav.htm", "");
    }
}
