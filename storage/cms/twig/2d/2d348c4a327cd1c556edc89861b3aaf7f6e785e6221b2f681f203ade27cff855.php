<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/home/welcome.htm */
class __TwigTemplate_22b8cf327ca85422544673d92eb982581a4726b36407ee7f51e294823939fbe3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"welcome-home\">
    <div class=\"container\">
        <div class=\"row justify-content-around\">
            <div class=\"col-12 col-lg-8 align-self-center\">
                <h1 class=\"mb-3 h1\">Welcome to Kate Bull Therapy</h1>
                ";
        // line 6
        echo ($context["welcome_message"] ?? null);
        echo "
            </div>
            <div class=\"col-12 col-lg-4 align-self-center mt-5 mt-lg-0\">
                <a href=\"";
        // line 9
        echo $this->env->getExtension('System\Twig\Extension')->mediaFilter(($context["welcome_image"] ?? null));
        echo "\" data-lightbox=\"welcome\">
                    <div class=\"welcome-image\" style=\" background: url('";
        // line 10
        echo $this->env->getExtension('System\Twig\Extension')->mediaFilter(($context["welcome_image"] ?? null));
        echo "') no-repeat center center; background-size: cover;\"></div>
                </a>
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/home/welcome.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 10,  32 => 9,  26 => 6,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"welcome-home\">
    <div class=\"container\">
        <div class=\"row justify-content-around\">
            <div class=\"col-12 col-lg-8 align-self-center\">
                <h1 class=\"mb-3 h1\">Welcome to Kate Bull Therapy</h1>
                {{welcome_message| raw}}
            </div>
            <div class=\"col-12 col-lg-4 align-self-center mt-5 mt-lg-0\">
                <a href=\"{{welcome_image | media}}\" data-lightbox=\"welcome\">
                    <div class=\"welcome-image\" style=\" background: url('{{welcome_image | media}}') no-repeat center center; background-size: cover;\"></div>
                </a>
            </div>
        </div>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/home/welcome.htm", "");
    }
}
