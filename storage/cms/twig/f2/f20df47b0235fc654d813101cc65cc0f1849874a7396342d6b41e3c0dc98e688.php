<?php

/* /home/vagrant/projects/walk-and-talk/plugins/martin/forms/components/emptyform/default.htm */
class __TwigTemplate_653deda57f59f07a77020201e8bed1bc218c95ea9cb4c714907ccb99f893294f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"contact-form\">
    <form data-request=\"";
        // line 2
        echo twig_escape_filter($this->env, ($context["__SELF__"] ?? null), "html", null, true);
        echo "::onFormSubmit\">
        <div class=\"row\">
            <div class=\"col-lg-12\">
                ";
        // line 5
        echo call_user_func_array($this->env->getFunction('form_token')->getCallable(), array("token"));
        echo "
                <div id=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["__SELF__"] ?? null), "html", null, true);
        echo "_forms_flash\"></div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-lg-6\">
                <div class=\"form-group\">
                    <label for=\"name\">Your Name *</label>
                    <input name=\"name\" id=\"name\" class=\"form-control\" required>
                </div>
                <div class=\"form-group\">
                    <label for=\"email\">Your Email *</label>
                    <input name=\"email\" type=\"email\" id=\"email\" class=\"form-control\" required>
                </div>
                <div class=\"form-group\">
                    <label for=\"phone\">Your Phone</label>
                    <input name=\"phone\" type=\"tel\" id=\"phone\" class=\"form-control\">
                </div>
            </div>
            <div class=\"col-lg-6\">
                <div class=\"form-group\">
                    <label for=\"message\">Your Message *</label>
                    <textarea name=\"message\" type=\"text\" id=\"message\" class=\"form-control\" required></textarea>
                </div>
                <div class=\"captcha\">
                    ";
        // line 30
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("@recaptcha"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 31
        echo "                </div>
                <button type=\"submit\" class=\"btn btn-custom\">Submit</button>
            </div>
        </div>
        <p class=\"mt-3\">* Required fields</p>
    </form>
</div>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/plugins/martin/forms/components/emptyform/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 31,  59 => 30,  32 => 6,  28 => 5,  22 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"contact-form\">
    <form data-request=\"{{ __SELF__ }}::onFormSubmit\">
        <div class=\"row\">
            <div class=\"col-lg-12\">
                {{ form_token() }}
                <div id=\"{{ __SELF__ }}_forms_flash\"></div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-lg-6\">
                <div class=\"form-group\">
                    <label for=\"name\">Your Name *</label>
                    <input name=\"name\" id=\"name\" class=\"form-control\" required>
                </div>
                <div class=\"form-group\">
                    <label for=\"email\">Your Email *</label>
                    <input name=\"email\" type=\"email\" id=\"email\" class=\"form-control\" required>
                </div>
                <div class=\"form-group\">
                    <label for=\"phone\">Your Phone</label>
                    <input name=\"phone\" type=\"tel\" id=\"phone\" class=\"form-control\">
                </div>
            </div>
            <div class=\"col-lg-6\">
                <div class=\"form-group\">
                    <label for=\"message\">Your Message *</label>
                    <textarea name=\"message\" type=\"text\" id=\"message\" class=\"form-control\" required></textarea>
                </div>
                <div class=\"captcha\">
                    {% partial '@recaptcha' %}
                </div>
                <button type=\"submit\" class=\"btn btn-custom\">Submit</button>
            </div>
        </div>
        <p class=\"mt-3\">* Required fields</p>
    </form>
</div>", "/home/vagrant/projects/walk-and-talk/plugins/martin/forms/components/emptyform/default.htm", "");
    }
}
