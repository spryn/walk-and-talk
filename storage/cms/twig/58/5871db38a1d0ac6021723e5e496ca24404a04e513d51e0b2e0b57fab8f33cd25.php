<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/cta.htm */
class __TwigTemplate_a33eef51fa6c8a021453b05d6f03d9fa4c573939eda56d8b1b098ad453b775a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"cta\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12\">
                <div class=\"row contact-cta\">
                    <div class=\"col-12 col-sm-6 col-md-8 d-flex align-items-center\">
                        <h1 class=\"h1\">Want to know more?</h1>
                    </div>
                    <div class=\"col-12 col-sm-6 col-md-4 d-flex align-items-center mt-3 mt-sm-0\">
                        <a href=\"/contact\" class=\"btn btn-custom\">Get in touch!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/cta.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"cta\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12\">
                <div class=\"row contact-cta\">
                    <div class=\"col-12 col-sm-6 col-md-8 d-flex align-items-center\">
                        <h1 class=\"h1\">Want to know more?</h1>
                    </div>
                    <div class=\"col-12 col-sm-6 col-md-4 d-flex align-items-center mt-3 mt-sm-0\">
                        <a href=\"/contact\" class=\"btn btn-custom\">Get in touch!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/cta.htm", "");
    }
}
