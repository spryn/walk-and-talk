<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/cookie-policy.htm */
class __TwigTemplate_e7636a6dd7099a01495940a9447a2de097b9c7e0a452e1258bb6bd6ab5dafd09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"header text-center d-flex\">
    <div class=\"container my-auto\">
        <div class=\"row\">
            <div class=\"col-xl-12 mx-auto\">
                <div class=\"header-title\">Cookie Policy</div>
            </div>
        </div>
    </div>
    <img src=\"";
        // line 9
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/cloud-bottom.png");
        echo "\" class=\"w-100 cloud cloud-bottom\" alt=\"Cloud\">
</header>

<section id=\"cookies\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12\">
                <h1>What are cookies</h1>
                <p>As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience.
                    This page describes what information they gather, how we use it and why we sometimes need to store these cookies. We will also share how you can prevent these cookies from
                    being stored however this may downgrade or 'break' certain elements of the sites functionality.</p>
                <p>For more general information on cookies see the <a href=\"https://en.wikipedia.org/wiki/HTTP_cookie\" target=\"_blank\">Wikipedia article on HTTP Cookies</a>.</p>
                <h1 class=\"mt-5\">How we use cookies</h1>
                <p>We use cookies for a variety of reasons detailed below. Unfortunately is most cases there are no industry standard options for disabling cookies without completely disabling
                    the functionality and features they add to this site. It is recommended that you leave on all cookies if you are not sure whether you need them or not in case they are used
                    to provide a service that you use.</p>
                <h1 class=\"mt-5\">Disabling cookies</h1>
                <p>You can prevent the setting of cookies by adjusting the settings on your browser (see your browser Help for how to do this). Be aware that disabling cookies will affect the
                    functionality of this and many other websites that you visit. Disabling cookies will usually result in also disabling certain functionality and features of the this site.
                    Therefore it is recommended that you do not disable cookies.</p>
                <h1 class=\"mt-5\">The cookies we set</h1>
                <p>When you submit data to through a form such as those found on contact pages or comment forms cookies may be set to remember your user details for future correspondence.</p>
                <h1 class=\"mt-5\">Third party cookies</h1>
                <p>In some special cases we also use cookies provided by trusted third parties. The following section details which third party cookies you might encounter through this
                    site.</p>
                <p>This site uses Google Analytics which is one of the most widespread and trusted analytics solution on the web for helping us to understand how you use the site and ways that
                    we can improve your experience. These cookies may track things such as how long you spend on the site and the pages that you visit so we can continue to produce engaging
                    content.</p>
                <p>For more information on Google Analytics cookies, see the <a href=\"https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage\" target=\"_blank\">official
                    Google Analytics page</a>.</p>
                <p>From time to time we test new features and make subtle changes to the way that the site is delivered. When we are still testing new features these cookies may be used to
                    ensure that you receive a consistent experience whilst on the site whilst ensuring we understand which optimisations our users appreciate the most.</p>
                <p>In some cases we may provide you with custom content based on what you tell us about yourself either directly or indirectly by linking a social media account. These types of
                    cookies simply allow us to provide you with content that we feel may be of interest to you.</p>
                <h1 class=\"mt-5\">More information</h1>
                <p>Hopefully that has clarified things for you and as was previously mentioned if there is something that you aren't sure whether you need or not it's usually safer to leave
                    cookies enabled in case it does interact with one of the features you use on our site. However if you are still looking for more information then you can contact us through
                    one of our preferred contact methods.</p>
                <p>Email: <a href=\"mailto:info@walkandtalknortheast.com\">info@walkandtalknortheast.com</a></p>
                <p>Phone: <a href=\"tel://07958787514\">07958787514</a></p>
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/cookie-policy.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 9,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"header text-center d-flex\">
    <div class=\"container my-auto\">
        <div class=\"row\">
            <div class=\"col-xl-12 mx-auto\">
                <div class=\"header-title\">Cookie Policy</div>
            </div>
        </div>
    </div>
    <img src=\"{{ 'assets/img/cloud-bottom.png'|theme }}\" class=\"w-100 cloud cloud-bottom\" alt=\"Cloud\">
</header>

<section id=\"cookies\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12\">
                <h1>What are cookies</h1>
                <p>As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience.
                    This page describes what information they gather, how we use it and why we sometimes need to store these cookies. We will also share how you can prevent these cookies from
                    being stored however this may downgrade or 'break' certain elements of the sites functionality.</p>
                <p>For more general information on cookies see the <a href=\"https://en.wikipedia.org/wiki/HTTP_cookie\" target=\"_blank\">Wikipedia article on HTTP Cookies</a>.</p>
                <h1 class=\"mt-5\">How we use cookies</h1>
                <p>We use cookies for a variety of reasons detailed below. Unfortunately is most cases there are no industry standard options for disabling cookies without completely disabling
                    the functionality and features they add to this site. It is recommended that you leave on all cookies if you are not sure whether you need them or not in case they are used
                    to provide a service that you use.</p>
                <h1 class=\"mt-5\">Disabling cookies</h1>
                <p>You can prevent the setting of cookies by adjusting the settings on your browser (see your browser Help for how to do this). Be aware that disabling cookies will affect the
                    functionality of this and many other websites that you visit. Disabling cookies will usually result in also disabling certain functionality and features of the this site.
                    Therefore it is recommended that you do not disable cookies.</p>
                <h1 class=\"mt-5\">The cookies we set</h1>
                <p>When you submit data to through a form such as those found on contact pages or comment forms cookies may be set to remember your user details for future correspondence.</p>
                <h1 class=\"mt-5\">Third party cookies</h1>
                <p>In some special cases we also use cookies provided by trusted third parties. The following section details which third party cookies you might encounter through this
                    site.</p>
                <p>This site uses Google Analytics which is one of the most widespread and trusted analytics solution on the web for helping us to understand how you use the site and ways that
                    we can improve your experience. These cookies may track things such as how long you spend on the site and the pages that you visit so we can continue to produce engaging
                    content.</p>
                <p>For more information on Google Analytics cookies, see the <a href=\"https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage\" target=\"_blank\">official
                    Google Analytics page</a>.</p>
                <p>From time to time we test new features and make subtle changes to the way that the site is delivered. When we are still testing new features these cookies may be used to
                    ensure that you receive a consistent experience whilst on the site whilst ensuring we understand which optimisations our users appreciate the most.</p>
                <p>In some cases we may provide you with custom content based on what you tell us about yourself either directly or indirectly by linking a social media account. These types of
                    cookies simply allow us to provide you with content that we feel may be of interest to you.</p>
                <h1 class=\"mt-5\">More information</h1>
                <p>Hopefully that has clarified things for you and as was previously mentioned if there is something that you aren't sure whether you need or not it's usually safer to leave
                    cookies enabled in case it does interact with one of the features you use on our site. However if you are still looking for more information then you can contact us through
                    one of our preferred contact methods.</p>
                <p>Email: <a href=\"mailto:info@walkandtalknortheast.com\">info@walkandtalknortheast.com</a></p>
                <p>Phone: <a href=\"tel://07958787514\">07958787514</a></p>
            </div>
        </div>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/cookie-policy.htm", "");
    }
}
