<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/blog-post.htm */
class __TwigTemplate_5b19cec100d6084fca42233301c655f41756a282d305255692eb1b61aa8d07e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"post-page\">
    <div class=\"container\">
        ";
        // line 3
        $context["post"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["blogPost"] ?? null), "post", array());
        // line 4
        echo "
        <h1 class=\"mb-3 h1\">
            ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "categories", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 7
            echo "            <a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["category"], "url", array()), "html", null, true);
            echo "\" class=\"a-indoor\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["category"], "name", array()), "html", null, true);
            echo "</a>";
            if ( !twig_get_attribute($this->env, $this->getSourceContext(), $context["loop"], "last", array())) {
                echo ", ";
            }
            // line 8
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "        </h1>

        <ul class=\"list-inline blog-info\">
            <li class=\"blog-author list-inline-item\">";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "user", array()), "first_name", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "user", array()), "last_name", array()), "html", null, true);
        echo "</li>
            <li class=\"list-inline-item\">-</li>
            <li class=\"blog-date list-inline-item\">";
        // line 14
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "published_at", array()), "M d, Y"), "html", null, true);
        echo "</li>
        </ul>

        <div class=\"mt-5\">
            <div class=\"content\">
                <p>";
        // line 19
        echo twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "content_html", array());
        echo "</p>
            </div>
        </div>
    </div>
</section>

<section id=\"sharing\">
    <div class=\"container\">
        <div class=\"blog-share-section text-center mt-5\">
            <div class=\"col-xl-12\">
                <h1 class=\"title\">Share this post:</h1>
                <div class=\"row mt-5\">
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://www.facebook.com/sharer/sharer.php?u=";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "url", array()), "html", null, true);
        echo "\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"";
        // line 35
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/icons/facebook.svg");
        echo "\" alt=\"Facebook\">
                                    <h2>Facebook</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 mt-3 mt-sm-0\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://twitter.com/home?status=";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "url", array()), "html", null, true);
        echo "\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"";
        // line 45
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/icons/twitter.svg");
        echo "\" alt=\"Twitter\">
                                    <h2>Twitter</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 mt-3 mt-sm-3 mt-md-0\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://plus.google.com/share?url=";
        // line 53
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "url", array()), "html", null, true);
        echo "\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"";
        // line 55
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/icons/google.svg");
        echo "\" alt=\"Google\">
                                    <h2>Google</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 mt-3 mt-sm-3 mt-md-0\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://www.linkedin.com/shareArticle?mini=true&url=";
        // line 63
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "url", array()), "html", null, true);
        echo "&title=";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "title", array()), "html", null, true);
        echo "&summary=&source=\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"";
        // line 65
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/icons/linkedin.svg");
        echo "\" alt=\"LinkedIn\">
                                    <h2>LinkedIn</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/blog-post.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 65,  154 => 63,  143 => 55,  138 => 53,  127 => 45,  122 => 43,  111 => 35,  106 => 33,  89 => 19,  81 => 14,  74 => 12,  69 => 9,  55 => 8,  46 => 7,  29 => 6,  25 => 4,  23 => 3,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"post-page\">
    <div class=\"container\">
        {% set post = blogPost.post %}

        <h1 class=\"mb-3 h1\">
            {% for category in post.categories %}
            <a href=\"{{ category.url }}\" class=\"a-indoor\">{{ category.name }}</a>{% if not loop.last %}, {% endif %}
            {% endfor %}
        </h1>

        <ul class=\"list-inline blog-info\">
            <li class=\"blog-author list-inline-item\">{{ post.user.first_name }} {{ post.user.last_name }}</li>
            <li class=\"list-inline-item\">-</li>
            <li class=\"blog-date list-inline-item\">{{ post.published_at|date('M d, Y') }}</li>
        </ul>

        <div class=\"mt-5\">
            <div class=\"content\">
                <p>{{ post.content_html|raw }}</p>
            </div>
        </div>
    </div>
</section>

<section id=\"sharing\">
    <div class=\"container\">
        <div class=\"blog-share-section text-center mt-5\">
            <div class=\"col-xl-12\">
                <h1 class=\"title\">Share this post:</h1>
                <div class=\"row mt-5\">
                    <div class=\"col-md-3 col-sm-6\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://www.facebook.com/sharer/sharer.php?u={{ post.url }}\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"{{ 'assets/img/icons/facebook.svg'|theme }}\" alt=\"Facebook\">
                                    <h2>Facebook</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 mt-3 mt-sm-0\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://twitter.com/home?status={{ post.url }}\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"{{ 'assets/img/icons/twitter.svg'|theme }}\" alt=\"Twitter\">
                                    <h2>Twitter</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 mt-3 mt-sm-3 mt-md-0\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://plus.google.com/share?url={{ post.url }}\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"{{ 'assets/img/icons/google.svg'|theme }}\" alt=\"Google\">
                                    <h2>Google</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 mt-3 mt-sm-3 mt-md-0\">
                        <div class=\"share-container\">
                            <a class=\"share-details a-indoor\" href=\"https://www.linkedin.com/shareArticle?mini=true&url={{ post.url }}&title={{ post.title }}&summary=&source=\" target=\"_blank\">
                                <div class=\"box\">
                                    <img class=\"mb-3\" src=\"{{ 'assets/img/icons/linkedin.svg'|theme }}\" alt=\"LinkedIn\">
                                    <h2>LinkedIn</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/pages/blog-post.htm", "");
    }
}
