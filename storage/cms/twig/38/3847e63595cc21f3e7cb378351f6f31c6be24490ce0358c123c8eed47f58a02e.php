<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/layouts/home.htm */
class __TwigTemplate_1ab39ab3284b4109bd51cc81c6a35e5e62e9569cf4428328ad933cb5c5711972 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "







<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0\">
    <meta name=\"description\" content=\"I believe that everyone has the potential to have good mental health and that they know themselves best and when given the right support they are able to achieve personal growth.\"/>
    <meta name=\"keywords\"
          content=\"\"/>
    <meta name=\"name\" itemprop=\"name\" content=\"http://katebulltherapy.com\">

    <title>";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo " | Kate Bull Therapy</title>

    <link rel=\"shortcut icon\" href=\"";
        // line 22
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/favicon.ico");
        echo "\" type=\"image/x-icon\">
    <link rel=\"icon\" href=\"";
        // line 23
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/favicon.ico");
        echo "\" type=\"image/x-icon\">

    <link rel=\"stylesheet\" href=\"";
        // line 25
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/font-awesome.min.css");
        echo "\">
    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\">
    <link href=\"https://fonts.googleapis.com/css?family=Lora:700|Work+Sans:300,400\" rel=\"stylesheet\">

    <!-- Custom CSS -->
    <link rel=\"stylesheet\" href=\"";
        // line 30
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/app.css");
        echo "\">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->

</head>

<body>

";
        // line 43
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("nav"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 44
        echo "
";
        // line 45
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("home/header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 46
        echo "
";
        // line 47
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("home/welcome"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 48
        echo "
";
        // line 49
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("home/about"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 50
        echo "
";
        // line 51
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("home/issues"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 52
        echo "
";
        // line 53
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("home/outdoor"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 54
        echo "
";
        // line 55
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("cta"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 56
        echo "
";
        // line 57
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 58
        echo "
<!-- Scripts -->
<script src=\"";
        // line 60
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/javascript/app.js");
        echo "\"></script>
";
        // line 61
        echo '<script src="'. Request::getBasePath()
                .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
        echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras.css">'.PHP_EOL;
        // line 62
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 63
        echo "
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/layouts/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 63,  146 => 62,  139 => 61,  135 => 60,  131 => 58,  127 => 57,  124 => 56,  120 => 55,  117 => 54,  113 => 53,  110 => 52,  106 => 51,  103 => 50,  99 => 49,  96 => 48,  92 => 47,  89 => 46,  85 => 45,  82 => 44,  78 => 43,  62 => 30,  54 => 25,  49 => 23,  45 => 22,  40 => 20,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("







<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0\">
    <meta name=\"description\" content=\"I believe that everyone has the potential to have good mental health and that they know themselves best and when given the right support they are able to achieve personal growth.\"/>
    <meta name=\"keywords\"
          content=\"\"/>
    <meta name=\"name\" itemprop=\"name\" content=\"http://katebulltherapy.com\">

    <title>{{ this.page.title }} | Kate Bull Therapy</title>

    <link rel=\"shortcut icon\" href=\"{{ 'assets/img/favicon.ico'|theme }}\" type=\"image/x-icon\">
    <link rel=\"icon\" href=\"{{ 'assets/img/favicon.ico'|theme }}\" type=\"image/x-icon\">

    <link rel=\"stylesheet\" href=\"{{ 'assets/css/font-awesome.min.css'|theme }}\">
    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\">
    <link href=\"https://fonts.googleapis.com/css?family=Lora:700|Work+Sans:300,400\" rel=\"stylesheet\">

    <!-- Custom CSS -->
    <link rel=\"stylesheet\" href=\"{{ 'assets/css/app.css'|theme }}\">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->

</head>

<body>

{% partial 'nav' %}

{% partial 'home/header' %}

{% partial 'home/welcome' %}

{% partial 'home/about' %}

{% partial 'home/issues' %}

{% partial 'home/outdoor' %}

{% partial 'cta' %}

{% partial 'footer' %}

<!-- Scripts -->
<script src=\"{{ 'assets/javascript/app.js'|theme }}\"></script>
{% framework extras %}
{% scripts %}

</body>
</html>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/layouts/home.htm", "");
    }
}
