<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/issues/key-issues.htm */
class __TwigTemplate_1baf27556e11345e3831591187e4d7521fe727f0866d2b27131ce46864e3fcab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"key-issues-list\">
    <div class=\"container\">
        <h1 class=\"mb-5 h1\">Key Issues</h1>
        <div class=\"row justify-content-center\">
            ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["issues"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["issue"]) {
            // line 6
            echo "            <div class=\"col-12 col-sm-6 mb-3\">
                <div id=\"issue-box\" data-children=\".issue-box\">
                    <div class=\"issue-box\">
                        <a class=\"a-indoor\" data-toggle=\"collapse\" data-parent=\"#issue-box\" href=\"#issue-box";
            // line 9
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
            echo "\" aria-expanded=\"true\" aria-controls=\"issue-box";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
            echo "\">
                            <h2 class=\"text-center issue-link\">";
            // line 10
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["issue"], "name", array()), "html", null, true);
            echo "</h2>
                        </a>
                        <div id=\"issue-box";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["loop"], "index", array()), "html", null, true);
            echo "\" class=\"collapse\" role=\"tabpanel\">
                            <hr>
                            <p class=\"mt-3 mb-0\">";
            // line 14
            echo twig_get_attribute($this->env, $this->getSourceContext(), $context["issue"], "info", array());
            echo "</p>
                        </div>
                    </div>
                </div>
            </div>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['issue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/issues/key-issues.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 20,  63 => 14,  58 => 12,  53 => 10,  47 => 9,  42 => 6,  25 => 5,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"key-issues-list\">
    <div class=\"container\">
        <h1 class=\"mb-5 h1\">Key Issues</h1>
        <div class=\"row justify-content-center\">
            {% for issue in issues %}
            <div class=\"col-12 col-sm-6 mb-3\">
                <div id=\"issue-box\" data-children=\".issue-box\">
                    <div class=\"issue-box\">
                        <a class=\"a-indoor\" data-toggle=\"collapse\" data-parent=\"#issue-box\" href=\"#issue-box{{loop.index}}\" aria-expanded=\"true\" aria-controls=\"issue-box{{loop.index}}\">
                            <h2 class=\"text-center issue-link\">{{issue.name}}</h2>
                        </a>
                        <div id=\"issue-box{{loop.index}}\" class=\"collapse\" role=\"tabpanel\">
                            <hr>
                            <p class=\"mt-3 mb-0\">{{issue.info|raw}}</p>
                        </div>
                    </div>
                </div>
            </div>
            {% endfor %}
        </div>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/issues/key-issues.htm", "");
    }
}
