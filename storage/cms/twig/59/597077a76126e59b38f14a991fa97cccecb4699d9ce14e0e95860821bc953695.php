<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/home/header.htm */
class __TwigTemplate_ce7f8b42d658b612495f14b6f1943760cfe0babfaaa3940b46527d69def47517 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"header header-home\">
    <div class=\"text-center d-flex\">
        <div class=\"container my-auto\">
            <div class=\"col-12 mx-auto\">
                <div class=\"header-title-home\">Kate Bull Therapy</div>
            </div>
            <div class=\"col-12 col-lg-8 mx-auto mt-5\">
                <div class=\"row text-center header-cta\">
                    <div class=\"col-12\">
                        <a href=\"/key-issues\" class=\"a-indoor\">
                            <h2>View Key Issues</h2>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/home/header.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"header header-home\">
    <div class=\"text-center d-flex\">
        <div class=\"container my-auto\">
            <div class=\"col-12 mx-auto\">
                <div class=\"header-title-home\">Kate Bull Therapy</div>
            </div>
            <div class=\"col-12 col-lg-8 mx-auto mt-5\">
                <div class=\"row text-center header-cta\">
                    <div class=\"col-12\">
                        <a href=\"/key-issues\" class=\"a-indoor\">
                            <h2>View Key Issues</h2>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/home/header.htm", "");
    }
}
