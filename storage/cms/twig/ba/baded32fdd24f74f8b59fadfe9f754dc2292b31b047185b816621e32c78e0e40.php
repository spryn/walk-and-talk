<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/blog/post-header.htm */
class __TwigTemplate_12b3a09d0b83697a927d7cda034250280b062e9fa76fdfda20db66f78736a97f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"header header-blog-post\" style=\"background-image: url('";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "featured_images", array()), 0, array(), "array"), "path", array()), "html", null, true);
        echo "');\">
    <div class=\"text-center d-flex\">
        <div class=\"container my-auto\">
            <div class=\"row\">
                <div class=\"col-12 mx-auto\">
                    <div class=\"header-title-home\">";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "title", array()), "html", null, true);
        echo "</div>
                </div>
            </div>
        </div>
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/blog/post-header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 6,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"header header-blog-post\" style=\"background-image: url('{{post.featured_images[0].path}}');\">
    <div class=\"text-center d-flex\">
        <div class=\"container my-auto\">
            <div class=\"row\">
                <div class=\"col-12 mx-auto\">
                    <div class=\"header-title-home\">{{ post.title }}</div>
                </div>
            </div>
        </div>
    </div>
</header>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/blog/post-header.htm", "");
    }
}
