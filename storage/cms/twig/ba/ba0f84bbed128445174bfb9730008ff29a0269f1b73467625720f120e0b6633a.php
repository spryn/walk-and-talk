<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/events/events-list.htm */
class __TwigTemplate_aa97fd8cf4843243b1fb89d4d9e66027c940d39eb72ab00485d3fe8bcf5fb018 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["records"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderList"] ?? null), "records", array());
        // line 2
        $context["displayColumn"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderList"] ?? null), "displayColumn", array());
        // line 3
        $context["noRecordsMessage"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderList"] ?? null), "noRecordsMessage", array());
        // line 4
        $context["detailsPage"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderList"] ?? null), "detailsPage", array());
        // line 5
        $context["detailsKeyColumn"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderList"] ?? null), "detailsKeyColumn", array());
        // line 6
        $context["detailsUrlParameter"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderList"] ?? null), "detailsUrlParameter", array());
        // line 7
        echo "
<section id=\"events-list\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-12 col-md-10 mx-auto\">
                <div class=\"upcoming\">
                    <h1 class=\"mb-3 h1\">Upcoming Events</h1>
                    <div class=\"row\">
                        ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 16
            echo "                        ";
            if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], "date", array()) >= twig_date_format_filter($this->env, "now", "Y-m-d"))) {
                // line 17
                echo "                        <div class=\"col-md-12 col-xl-6\">
                            <div class=\"post mb-5\">
                                <div class=\"blog-image-container\">
                                    <a href=\"";
                // line 20
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(($context["detailsPage"] ?? null), array(($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], ($context["detailsKeyColumn"] ?? null))));
                echo "\">
                                        ";
                // line 21
                if ((null === twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], "event_image", array()))) {
                    // line 22
                    echo "                                        <div style=\"background-image: url(";
                    echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/event-placeholder.png");
                    echo ");\" class=\"blog-image\"></div>
                                        ";
                } else {
                    // line 24
                    echo "                                        <div style=\"background-image: url('";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], "event_image", array()), "path", array()), "html", null, true);
                    echo "');\" class=\"blog-image\"></div>
                                        ";
                }
                // line 26
                echo "                                    </a>
                                </div>
                                <div class=\"post-info text-center\">
                                    <h2 class=\"section-heading\"><a href=\"";
                // line 29
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "url", array()), "html", null, true);
                echo "\" class=\"a-indoor\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], "name", array()), "html", null, true);
                echo "</a></h2>
                                    <div class=\"event-info\">
                                        <p>";
                // line 31
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], "short_description", array()), "html", null, true);
                echo "</p>
                                        <p class=\"text-muted\">";
                // line 32
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], "date", array()), "d-m-Y"), "html", null, true);
                echo "</p>
                                    </div>
                                    <a href=\"";
                // line 34
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(($context["detailsPage"] ?? null), array(($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], ($context["detailsKeyColumn"] ?? null))));
                echo "\" class=\"btn btn-custom mt-3\">View</a>
                                </div>
                            </div>
                        </div>
                        ";
            }
            // line 39
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "                    </div>
                </div>
                <div class=\"past mt-5\">
                    <h1 class=\"mb-3 h1\">Past Events</h1>
                    <div class=\"row\">
                        ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["records"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["record"]) {
            // line 46
            echo "                        ";
            if ((twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], "date", array()) < twig_date_format_filter($this->env, "now", "Y-m-d"))) {
                // line 47
                echo "                        <div class=\"col-md-12 col-xl-6\">
                            <div class=\"post mb-5\">
                                <div class=\"blog-image-container\">
                                    <a href=\"";
                // line 50
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(($context["detailsPage"] ?? null), array(($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], ($context["detailsKeyColumn"] ?? null))));
                echo "\">
                                        ";
                // line 51
                if ((null === twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], "event_image", array()))) {
                    // line 52
                    echo "                                        <div style=\"background-image: url(";
                    echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/event-placeholder.png");
                    echo ");\" class=\"blog-image\"></div>
                                        ";
                } else {
                    // line 54
                    echo "                                        <div style=\"background-image: url('";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], "event_image", array()), "path", array()), "html", null, true);
                    echo "');\" class=\"blog-image\"></div>
                                        ";
                }
                // line 56
                echo "                                    </a>
                                </div>
                                <div class=\"post-info text-center\">
                                    <h2 class=\"section-heading\"><a href=\"";
                // line 59
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["post"] ?? null), "url", array()), "html", null, true);
                echo "\" class=\"a-indoor\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], "name", array()), "html", null, true);
                echo "</a></h2>
                                    <div class=\"event-info\">
                                        <p>";
                // line 61
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], "short_description", array()), "html", null, true);
                echo "</p>
                                        <p class=\"text-muted\">";
                // line 62
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], "date", array()), "d-m-Y"), "html", null, true);
                echo "</p>
                                    </div>
                                    <a href=\"";
                // line 64
                echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter(($context["detailsPage"] ?? null), array(($context["detailsUrlParameter"] ?? null) => twig_get_attribute($this->env, $this->getSourceContext(), $context["record"], ($context["detailsKeyColumn"] ?? null))));
                echo "\" class=\"btn btn-custom mt-3\">View</a>
                                </div>
                            </div>
                        </div>
                        ";
            }
            // line 69
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['record'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 70
        echo "                    </div>
                </div>
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/events/events-list.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 70,  172 => 69,  164 => 64,  159 => 62,  155 => 61,  148 => 59,  143 => 56,  137 => 54,  131 => 52,  129 => 51,  125 => 50,  120 => 47,  117 => 46,  113 => 45,  106 => 40,  100 => 39,  92 => 34,  87 => 32,  83 => 31,  76 => 29,  71 => 26,  65 => 24,  59 => 22,  57 => 21,  53 => 20,  48 => 17,  45 => 16,  41 => 15,  31 => 7,  29 => 6,  27 => 5,  25 => 4,  23 => 3,  21 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set records = builderList.records %}
{% set displayColumn = builderList.displayColumn %}
{% set noRecordsMessage = builderList.noRecordsMessage %}
{% set detailsPage = builderList.detailsPage %}
{% set detailsKeyColumn = builderList.detailsKeyColumn %}
{% set detailsUrlParameter = builderList.detailsUrlParameter %}

<section id=\"events-list\">
    <div class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-12 col-md-10 mx-auto\">
                <div class=\"upcoming\">
                    <h1 class=\"mb-3 h1\">Upcoming Events</h1>
                    <div class=\"row\">
                        {% for record in records %}
                        {% if record.date >= \"now\"|date(\"Y-m-d\") %}
                        <div class=\"col-md-12 col-xl-6\">
                            <div class=\"post mb-5\">
                                <div class=\"blog-image-container\">
                                    <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\">
                                        {% if record.event_image is null %}
                                        <div style=\"background-image: url({{ 'assets/img/event-placeholder.png'|theme }});\" class=\"blog-image\"></div>
                                        {% else %}
                                        <div style=\"background-image: url('{{record.event_image.path}}');\" class=\"blog-image\"></div>
                                        {% endif %}
                                    </a>
                                </div>
                                <div class=\"post-info text-center\">
                                    <h2 class=\"section-heading\"><a href=\"{{ post.url }}\" class=\"a-indoor\">{{record.name}}</a></h2>
                                    <div class=\"event-info\">
                                        <p>{{record.short_description}}</p>
                                        <p class=\"text-muted\">{{record.date|date(\"d-m-Y\")}}</p>
                                    </div>
                                    <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\" class=\"btn btn-custom mt-3\">View</a>
                                </div>
                            </div>
                        </div>
                        {% endif %}
                        {% endfor %}
                    </div>
                </div>
                <div class=\"past mt-5\">
                    <h1 class=\"mb-3 h1\">Past Events</h1>
                    <div class=\"row\">
                        {% for record in records %}
                        {% if record.date < \"now\"|date(\"Y-m-d\") %}
                        <div class=\"col-md-12 col-xl-6\">
                            <div class=\"post mb-5\">
                                <div class=\"blog-image-container\">
                                    <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\">
                                        {% if record.event_image is null %}
                                        <div style=\"background-image: url({{ 'assets/img/event-placeholder.png'|theme }});\" class=\"blog-image\"></div>
                                        {% else %}
                                        <div style=\"background-image: url('{{record.event_image.path}}');\" class=\"blog-image\"></div>
                                        {% endif %}
                                    </a>
                                </div>
                                <div class=\"post-info text-center\">
                                    <h2 class=\"section-heading\"><a href=\"{{ post.url }}\" class=\"a-indoor\">{{record.name}}</a></h2>
                                    <div class=\"event-info\">
                                        <p>{{record.short_description}}</p>
                                        <p class=\"text-muted\">{{record.date|date(\"d-m-Y\")}}</p>
                                    </div>
                                    <a href=\"{{ detailsPage|page({ (detailsUrlParameter): attribute(record, detailsKeyColumn) }) }}\" class=\"btn btn-custom mt-3\">View</a>
                                </div>
                            </div>
                        </div>
                        {% endif %}
                        {% endfor %}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/events/events-list.htm", "");
    }
}
