<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/layouts/issues.htm */
class __TwigTemplate_b37a5edc2a53c19785796111a4a174a0805d1f42af67bf9bf5c5404ac085e19c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), ($context["extraData"] ?? null), "issues", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["fields"]) {
            // line 4
            echo "

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fields'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "
<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0\">
    <meta name=\"description\" content=\"I believe that everyone has the potential to have good mental health and that they know themselves best and when given the right support they are able to achieve personal growth.\"/>
    <meta name=\"keywords\"
          content=\"\"/>
    <meta name=\"name\" itemprop=\"name\" content=\"http://katebulltherapy.com\">

    <title>";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo " | Kate Bull Therapy</title>

    <link rel=\"shortcut icon\" href=\"";
        // line 21
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/favicon.ico");
        echo "\" type=\"image/x-icon\">
    <link rel=\"icon\" href=\"";
        // line 22
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/favicon.ico");
        echo "\" type=\"image/x-icon\">

    <link rel=\"stylesheet\" href=\"";
        // line 24
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/font-awesome.min.css");
        echo "\">
    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\">
    <link href=\"https://fonts.googleapis.com/css?family=Lora:700|Work+Sans:300,400\" rel=\"stylesheet\">

    <!-- Custom CSS -->
    <link rel=\"stylesheet\" href=\"";
        // line 29
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/app.css");
        echo "\">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->

</head>

<body>

";
        // line 42
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("nav"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 43
        echo "
";
        // line 44
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("issues/header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 45
        echo "
";
        // line 46
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("issues/intro"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 47
        echo "
";
        // line 48
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("issues/key-issues"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 49
        echo "
";
        // line 50
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("issues/issues"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 51
        echo "
";
        // line 52
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("cta"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 53
        echo "
";
        // line 54
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 55
        echo "
<!-- Scripts -->
<script src=\"";
        // line 57
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/javascript/app.js");
        echo "\"></script>
";
        // line 58
        echo '<script src="'. Request::getBasePath()
                .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
        echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras.css">'.PHP_EOL;
        // line 59
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 60
        echo "
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/layouts/issues.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 60,  148 => 59,  141 => 58,  137 => 57,  133 => 55,  129 => 54,  126 => 53,  122 => 52,  119 => 51,  115 => 50,  112 => 49,  108 => 48,  105 => 47,  101 => 46,  98 => 45,  94 => 44,  91 => 43,  87 => 42,  71 => 29,  63 => 24,  58 => 22,  54 => 21,  49 => 19,  35 => 7,  27 => 4,  23 => 3,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("

{% for fields in extraData.issues %}


{% endfor %}

<!DOCTYPE html>
<html>
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0\">
    <meta name=\"description\" content=\"I believe that everyone has the potential to have good mental health and that they know themselves best and when given the right support they are able to achieve personal growth.\"/>
    <meta name=\"keywords\"
          content=\"\"/>
    <meta name=\"name\" itemprop=\"name\" content=\"http://katebulltherapy.com\">

    <title>{{ this.page.title }} | Kate Bull Therapy</title>

    <link rel=\"shortcut icon\" href=\"{{ 'assets/img/favicon.ico'|theme }}\" type=\"image/x-icon\">
    <link rel=\"icon\" href=\"{{ 'assets/img/favicon.ico'|theme }}\" type=\"image/x-icon\">

    <link rel=\"stylesheet\" href=\"{{ 'assets/css/font-awesome.min.css'|theme }}\">
    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\">
    <link href=\"https://fonts.googleapis.com/css?family=Lora:700|Work+Sans:300,400\" rel=\"stylesheet\">

    <!-- Custom CSS -->
    <link rel=\"stylesheet\" href=\"{{ 'assets/css/app.css'|theme }}\">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->

</head>

<body>

{% partial 'nav' %}

{% partial 'issues/header' %}

{% partial 'issues/intro' %}

{% partial 'issues/key-issues' %}

{% partial 'issues/issues' %}

{% partial 'cta' %}

{% partial 'footer' %}

<!-- Scripts -->
<script src=\"{{ 'assets/javascript/app.js'|theme }}\"></script>
{% framework extras %}
{% scripts %}

</body>
</html>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/layouts/issues.htm", "");
    }
}
