<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/contact/prices.htm */
class __TwigTemplate_17af5c6413abc114d103c13492a10d8af4b7849dfca8c3b371de1fea26b39484 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"prices\">
    <div class=\"container\">
        <h1 class=\"mb-3 h1\">Prices</h1>
        <div class=\"row text-center\">
            <div class=\"col-12 col-md-6\">
                <div class=\"price-box\">
                    <h2>Indoor Session</h2>
                    <strong class=\"mb-0\">50 minutes - £40</strong>
                </div>
            </div>
            <div class=\"col-12 col-md-6\">
                <div class=\"price-box mt-3 mt-md-0\">
                    <h2>Outdoor Session</h2>
                    <strong class=\"mb-0\">1 hour - £40</strong>
                </div>
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/contact/prices.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"prices\">
    <div class=\"container\">
        <h1 class=\"mb-3 h1\">Prices</h1>
        <div class=\"row text-center\">
            <div class=\"col-12 col-md-6\">
                <div class=\"price-box\">
                    <h2>Indoor Session</h2>
                    <strong class=\"mb-0\">50 minutes - £40</strong>
                </div>
            </div>
            <div class=\"col-12 col-md-6\">
                <div class=\"price-box mt-3 mt-md-0\">
                    <h2>Outdoor Session</h2>
                    <strong class=\"mb-0\">1 hour - £40</strong>
                </div>
            </div>
        </div>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/contact/prices.htm", "");
    }
}
