<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/contact/map.htm */
class __TwigTemplate_ae6629a8631d6ee751e676519591e94e9176b8e0efa546152eb586c5aab95984 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section>
    <div class=\"container\">
        <h1 class=\"mb-3 h1\">Visit me!</h1>
        <div id=\"map\"></div>
        <script>
            function initMap() {
                var location = {lat: 55.010653, lng: -1.449792};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: location,
                    scrollwheel: false,
                    gestureHandling: 'cooperative'
                });
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
            }
        </script>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/contact/map.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section>
    <div class=\"container\">
        <h1 class=\"mb-3 h1\">Visit me!</h1>
        <div id=\"map\"></div>
        <script>
            function initMap() {
                var location = {lat: 55.010653, lng: -1.449792};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: location,
                    scrollwheel: false,
                    gestureHandling: 'cooperative'
                });
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
            }
        </script>
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/contact/map.htm", "");
    }
}
