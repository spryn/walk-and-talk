<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/events/header.htm */
class __TwigTemplate_d2a671a610dd2471f2073e8d25779f1dc253d0229111cbb4be0a77d9015fbf46 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"header header-events text-center d-flex\">
    <div class=\"container my-auto\">
        <div class=\"row\">
            <div class=\"col-xl-10 mx-auto\">
                <div class=\"header-title-home\">Events</div>
            </div>
        </div>
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/events/header.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"header header-events text-center d-flex\">
    <div class=\"container my-auto\">
        <div class=\"row\">
            <div class=\"col-xl-10 mx-auto\">
                <div class=\"header-title-home\">Events</div>
            </div>
        </div>
    </div>
</header>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/events/header.htm", "");
    }
}
