<?php

/* /home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/issues/intro.htm */
class __TwigTemplate_132e961ce10c4178b24e0199782f1ee46e0c9a52a02b689110672a8c286ffcf2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"therapies-intro\">
    <div class=\"container\">
        <h1 class=\"mb-3 h1\">Indoor Practice</h1>
        ";
        // line 4
        echo ($context["issues_intro"] ?? null);
        echo "
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/issues/intro.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 4,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"therapies-intro\">
    <div class=\"container\">
        <h1 class=\"mb-3 h1\">Indoor Practice</h1>
        {{issues_intro| raw}}
    </div>
</section>", "/home/vagrant/projects/walk-and-talk/themes/walk-and-talk/partials/issues/intro.htm", "");
    }
}
