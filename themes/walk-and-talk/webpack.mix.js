let mix = require('laravel-mix');
let webpack = require('webpack');

mix.setPublicPath('./');

mix.webpackConfig({
    // devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
        ]
    },
    resolve: {
        // alias: {
        //     'jquery': 'jquery/dist/jquery.slim.min'
        // }
    },
    plugins: [
        new webpack.ProvidePlugin({
            // $: 'jquery',
            // jQuery: 'jquery',
            // 'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default']

        })
    ]
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'assets/javascript');
mix.sass('./resources/sass/app.scss', './assets/css').options({
    processCssUrls: false
});
