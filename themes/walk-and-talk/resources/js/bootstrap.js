window._ = require('lodash');

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap-sass');
    require('jquery.easing');
    require('lightbox2');

} catch (e) {
}
