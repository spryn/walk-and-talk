<?php namespace Bluecookie\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBluecookieEvents extends Migration
{
    public function up()
    {
        Schema::table('bluecookie_events_', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('bluecookie_events_', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
