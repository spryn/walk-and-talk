<?php namespace Bluecookie\Events\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBluecookieEvents extends Migration
{
    public function up()
    {
        Schema::create('bluecookie_events_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('short_description');
            $table->text('description');
            $table->dateTime('date');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bluecookie_events_');
    }
}
